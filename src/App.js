import React from "react";
import "./App.css";
import "semantic-ui-css/semantic.min.css";
import { Menu, Icon } from "semantic-ui-react";
import { Link, BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { About } from "./components/about";
import { Tea } from "./components/tea";

function App() {
	return (
		<Router>
			<Menu>
				<Menu.Item as={Link} to="/" active={false}>
					<Icon name="coffee" />
				</Menu.Item>
				<Menu.Item
					as={Link}
					to="/about"
					active={false}
					name="About the Project"
				/>
			</Menu>
			<Switch>
				<Route exact path="/">
					<Tea />
				</Route>
				<Route path="/about">
					<About />
				</Route>
			</Switch>
		</Router>
	);
}

export default App;
