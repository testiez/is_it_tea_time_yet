import React from "react";
import { Item, Segment } from "semantic-ui-react";

export const About = () => {
	return (
		<Item.Group as={Segment}>
			<Item >
				<Item.Content >
					<Item.Header >About the project</Item.Header>
					<Item.Description>
						<b>Is It Tea Time Yet</b> aims to to track whether you should or
						should not be having tea, helping millions of people in their
						everyday life and making the world a better place.
					</Item.Description>
				</Item.Content>
			</Item>
            <Item >
				<Item.Content >
					<Item.Header >About the Developer</Item.Header>
					<Item.Description>
						I'm a Tech enthusiast and a Data Engineering Wizard 
					</Item.Description>
				</Item.Content>
			</Item>
		</Item.Group>
	);
};
