import React, { useState, useEffect } from "react";
import { Segment, Header, Image } from "semantic-ui-react";
import tea from '../tea.jpg'

const Oui = () => (
    <>  
        <Header as="h1">Yes</Header>
        <Image src={tea} size='medium' rounded />
    </>
)

const Non = () => (
    <Header as="h1">Not Yet</Header>
)


export const Tea = () => {
	const checkTime = () => {
		const date = new Date();
		return date.getHours() === 15 && date.getMinutes() < 30;
	};
	const [mustBeUpdated, setMustBeUpdated] = useState(true);

	const [isTime, setIsTime] = useState();

	useEffect(() => {
		if (mustBeUpdated) {
			setMustBeUpdated(false);
			setInterval(() => {
				setMustBeUpdated(true);
			}, 10000);
            setIsTime(checkTime());
		}
	}, [mustBeUpdated]);

return <Segment>{isTime ? <Oui/> : <Non/>}</Segment>;
};
